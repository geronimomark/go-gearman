package main

import (
	"github.com/mikespook/gearman-go/client"
	"log"
	"os"
	"sync"

    "strconv"
    // "fmt"
    // "time"
    // "math/rand"

    "github.com/geronimomark/bg-listing-dispatcher/lib/models"
)

type Env struct {
    db models.Datastore
}

func main() {
	// Set the autoinc id generator
	// You can write your own id generator
	// by implementing IdGenerator interface.
	// client.IdGen = client.NewAutoIncId()

    log.Println("Client start...")

	c, err := client.New(client.Network, models.GEARMAN_HOST)
	if err != nil {
		log.Fatalln(err)
	}
	defer c.Close()
	c.ErrorHandler = func(e error) {
		log.Println(e)
		os.Exit(1)
	}

	jobHandler := func(resp *client.Response) {
		switch resp.DataType {
		case client.WorkException:
			fallthrough
		case client.WorkFail:
			fallthrough
		case client.WorkComplate:
			if data, err := resp.Result(); err == nil {
				log.Printf("RESULT: %v\n", data)
			} else {
				log.Printf("RESULT: %s\n", err)
			}
		case client.WorkWarning:
			fallthrough
		case client.WorkData:
			if data, err := resp.Update(); err == nil {
				log.Printf("UPDATE: %v\n", data)
			} else {
				log.Printf("UPDATE: %v, %s\n", data, err)
			}
		case client.WorkStatus:
			if data, err := resp.Status(); err == nil {
				log.Printf("STATUS: %v\n", data)
			} else {
				log.Printf("STATUS: %s\n", err)
			}
		default:
			log.Printf("UNKNOWN: %v", resp.Data)
		}
	}

    db, err := models.Connect()
    if err != nil {
		log.Fatalln(err)
	}

    env := &Env{db}

    log.Println("Get Queued jobs...")
    lgjobs, err := env.db.GetWhereStatus("queued")
    if err != nil {
		log.Fatalln(err)
	}

    for x := range lgjobs {

        log.Println("Dispatching Job:", lgjobs[x].JobId)
        jobid := []byte(strconv.Itoa(lgjobs[x].JobId))
        // params, err := c.Echo(jobid)

        _, err = c.Do(models.LISTING_GEN, jobid, client.JobNormal, jobHandler)
    	if err != nil {
    		log.Fatalln(err)
    	}
        // set status to dispatched
        err := env.db.Update(lgjobs[x], map[string]interface{}{"status": "dispatched"})
        if err != nil {
    		log.Fatalln(err)
    	}
        // time.Sleep(time.Second)

    }

	log.Println("Press Ctrl-C to exit ...")
	var mutex sync.Mutex
	mutex.Lock()
	mutex.Lock()
}
