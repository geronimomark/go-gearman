package main

import (
	"log"
	"net"
	"os"
    // "fmt"

    "strconv"
    "time"

	"github.com/mikespook/gearman-go/worker"
	"github.com/mikespook/golib/signal"

    "github.com/geronimomark/bg-listing-dispatcher/lib/models"
)

type Env struct {
    db models.Datastore
}

func Dispatch(job worker.Job) ([]byte, error) {

    db, err := models.Connect()
    if err != nil {
        log.Fatalln(err)
    }

    env := &Env{db}

    id, err := strconv.Atoi(string(job.Data()))
    log.Println("Inprogress: Data=", id, job)

    lgjob := &models.Lg_jobs{JobId: id, Status: ""}

    // Own the job id and set to inprogress
    // db.Exec("UPDATE lg_jobs SET status=? WHERE lg_job_id = ?", "inprogress", id)
    errdb := env.db.Update(lgjob, map[string]interface{}{"status": "inprogress"})
    if errdb != nil {
        log.Fatalln(errdb)
    }
    // db.Exec("UPDATE process_list SET job_id=? WHERE worker_name = ?", id, job.Fn())

    // timeout to simulate long process
    time.Sleep(10 * time.Second)

    // set to done
    log.Println("Done: Data=", id, job)
    // db.Exec("UPDATE lg_jobs SET status=? WHERE lg_job_id = ?", "done", id)
    errdb2 := env.db.Update(lgjob, map[string]interface{}{"status": "done"})
    if errdb2 != nil {
        log.Fatalln(errdb2)
    }
    // db.Exec("UPDATE process_list SET busy=? WHERE worker_name = ?", false, job.Fn())

    ret := []byte(string(id))

    return ret, nil
}

func main() {
	log.Println("Starting ...")
	defer log.Println("Shutdown complete!")

	w := worker.New(worker.Unlimited)
	defer w.Close()

	w.ErrorHandler = func(e error) {
		log.Println(e)
		if opErr, ok := e.(*net.OpError); ok {
			if !opErr.Temporary() {
				proc, err := os.FindProcess(os.Getpid())
				if err != nil {
					log.Println(err)
				}
				if err := proc.Signal(os.Interrupt); err != nil {
					log.Println(err)
				}
			}
		}
	}
	w.JobHandler = func(job worker.Job) error {
		log.Printf("Data=%s\n", job.Data())
		return nil
	}

	w.AddServer("tcp4", models.GEARMAN_HOST)
    w.AddFunc(models.LISTING_GEN, Dispatch, 5)
    // w.AddFunc("LISTING_GEN", Dispatch, worker.Unlimited)

	if err := w.Ready(); err != nil {
		log.Fatal(err)
		return
	}
	go w.Work()
	signal.Bind(os.Interrupt, func() uint { return signal.BreakExit })
	signal.Wait()
}
