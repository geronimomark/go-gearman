package models

import (
    _ "github.com/jinzhu/gorm/dialects/postgres"
)

var table = "lg_jobs"

type Lg_jobs struct {
    JobId    int     `gorm:"column:lg_job_id"`
    Status   string
}

func (db *DB) GetWhereStatus(status string) ([]*Lg_jobs, error) {
    lgjobs := make([]*Lg_jobs, 0)
    err := db.Where("status = ?", status).Find(&lgjobs).Error
    return lgjobs, err
}

func (db *DB) Update(lgjob *Lg_jobs, data map[string]interface{}) error {
    return db.Table(table).Where("lg_job_id = ?", lgjob.JobId).Updates(data).Error
}
