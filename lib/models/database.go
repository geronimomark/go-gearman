package models

import (
    "fmt"
    "log"
    // "os"

    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
    // TO DO: change to environment variables
    DB_USER     = "postgres" // os.Getenv("FOO")
    DB_PASSWORD = "mysecretpassword" // os.Getenv("FOO")
    DB_NAME     = "testgo" // os.Getenv("FOO")
    DB_HOST     = "172.17.0.2" // os.Getenv("FOO")
    GEARMAN_HOST= "127.0.0.1:4730" // os.Getenv("FOO")
    LISTING_GEN = "listing-gen" // os.Getenv("FOO")
)

type Datastore interface {
    GetWhereStatus(string) ([]*Lg_jobs, error)
    Update(*Lg_jobs, map[string]interface{}) error
}

type DB struct {
    *gorm.DB
}

func Connect() (*DB, error){
    log.Println("Connecting to postgres DB...")
    db, err := gorm.Open("postgres", fmt.Sprintf("user=%s password=%s host=%s dbname=%s sslmode=disable", DB_USER, DB_PASSWORD, DB_HOST, DB_NAME))
    if err != nil {
        return nil, err
    }
    return &DB{db}, err
}
